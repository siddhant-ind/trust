import React, {Component} from 'react';
import {
    Alert,
    View,
    ScrollView,
    AsyncStorage,
    TouchableOpacity,
    Text,
    Image,
    KeyboardAvoidingView,
    Linking,
    Platform,
    SafeAreaView,
    YellowBox,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';




export const Home = () => {
    return (
        <SafeAreaView style={{flex:1}}>
          <View style={{flex:1,backgroundColor:'#1A1F22'}}>
            <View style={{flex:0.2}}>
              <View style={{flex:0.5,backgroundColor:'#28313A',justifyContent:'flex-end',alignItems:'center'}}>
              <Image style={{maxWidth:'40%',maxHeight:'25%'}} resizeMode={'contain'} source={require('../../assets/logosmall.png')}  />
              </View>
              <View style={{flex:0.5,backgroundColor:'#28313A',justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}}>
                <View style={{backgroundColor:'white',width:50,height:50,borderRadius:25,marginLeft:20}}>
                </View>
                <Text style={{marginLeft:20,fontSize:15,color:'white'}}>
                    SABRI CHENIOUR{'\n'}@MPI{'\n'}Super Cable + Device
                  </Text>
              </View>
            </View>
            <View style={{flex:0.5,marginTop:50,alignItems:'center'}}>
              <View style={{height:50,width:340,backgroundColor:'#28313A',marginTop:10,flexDirection:'row',alignItems:'center'}}>
              <Image style={{maxWidth:'50%',maxHeight:'35%'}} resizeMode={'contain'} source={require('../../assets/bell.png')}  />
              
              <Text style={{fontSize:15,color:'white'}}>
                   Alerts
                  </Text>
                
              </View>
               <View style={{height:50,width:340,backgroundColor:'#28313A',marginTop:10,flexDirection:'row',alignItems:'center'}}>
               <Image style={{maxWidth:'50%',maxHeight:'35%'}} resizeMode={'contain'} source={require('../../assets/folder.png')}  />
               <Text style={{fontSize:15,color:'white'}}>
                    My work packages
                  </Text>
              </View>
              <View style={{height:50,width:340,backgroundColor:'#28313A',marginTop:10,flexDirection:'row',alignItems:'center'}}>
              <Image style={{maxWidth:'55%',maxHeight:'40%'}} resizeMode={'contain'} source={require('../../assets/Ellipse.png')}  />
              <Text style={{fontSize:15,color:'white'}}>
                    My blockers
                  </Text>
              </View>



            </View>


          </View>
        </SafeAreaView>
    );
  }
  
  


