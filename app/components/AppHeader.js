import React, {Component} from 'react';
import {Platform, TouchableOpacity, View} from 'react-native';
import Color from '../lib/Color';
import { LinearGradient } from 'expo-linear-gradient';


export default class AppHeader extends Component {

    constructor() {
        super();
    }

    render() {

        let colors = ['#28313A', '#28313A', '#28313A'];
        if (this.props.linearGradient !== undefined && this.props.linearGradient !== true) {
            colors = ['#ffffff', '#ffffff', '#ffffff'];
        }

        this.roundHeaderStyle = {};
        if (this.props.roundHeader) {
            this.roundHeaderStyle = {borderBottomLeftRadius: 25, borderBottomRightRadius: 25};
        }
        return (
            <LinearGradient
                start={{x: 1, y: 0}} end={{x: 0, y: 0}}
                colors={colors}
                style={[styles.customStyle, this.roundHeaderStyle, this.props.containerStyle]}
            >
                <TouchableOpacity onPress={this.props.leftPress}style={[styles.left, this.props.leftStyle]}>
                    {
                        this.props.left &&
                        this.props.left
                    }
                </TouchableOpacity>
                <View style={[styles.body, this.props.bodyStyle]}>
                    {
                        this.props.body &&
                        this.props.body
                    }
                </View>
                <View style={[styles.right, this.props.rightStyle]}>
                    {
                        this.props.right &&
                        this.props.right
                    }
                </View>
            </LinearGradient>
        );
    }
}

const styles = {
    customStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        height: Platform.OS === 'ios' ? 70 : 100,
        width: '100%',
        backgroundColor: '#ffffff',
        paddingLeft: 20,
        paddingRight: 40,
    },
    left: {
        flex: 1,
    },
    body: {
        flex: 2,
        alignItems: "center",
        justifyContent: "center"
    },
    right: {
        flex: 1,
        alignItems: 'flex-end',

    },
};
