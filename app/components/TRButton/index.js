import React from 'react';
import {ImageBackground, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator} from "react-native";
import {_images} from '../../lib/Images'
import Color from "../../lib/Color";
import {COLORS} from "../../configs/Constants";

export default class MCAButton extends React.Component {

  buttonWithBackgroundImage() {
    return (
      <View style={styles.buttonContainer}>
        <ImageBackground source={_images.Rectangle922}
                         style={[styles.backgroundImage, this.props.backgroundImageStyle]}>
          <TouchableOpacity disabled={this.props.loading || this.props.disabled}
                            style={[styles.button, this.props.buttonStyle]}
                            onPress={this.props.onPress}>
            {
              this.props.loading &&
              <ActivityIndicator color={COLORS.WHITE} size={'small'}/>
              ||
              <Text style={[styles.buttonText, this.props.textStyle]}>{this.props.text}</Text>
            }
          </TouchableOpacity>

        </ImageBackground>
      </View>
    );
  }

  buttonWithoutBackgroundImage() {
    return (
      <View style={[styles.buttonContainer, this.props.buttonContainer]}>
        <TouchableOpacity
          style={[{
            height: 45,
            width: 157,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            borderColor: COLORS.BORDER,
            borderRadius: 22.5,
          }, this.props.buttonStyle]}
          disabled={this.props.loading || this.props.disabled}
          onPress={this.props.onPress}
        >{
          this.props.loading &&
          <ActivityIndicator color={COLORS.WHITE} size={'small'}/>
          ||
          <Text style={[styles.buttonText, {color: Color.Secondary}, this.props.textStyle]}>
            {this.props.text}
          </Text>
        }
        </TouchableOpacity>
      </View>
    );
  }

  renderButton() {
    const {backgroundImage} = this.props;
    if (backgroundImage !== undefined && backgroundImage !== null) {
      return this.buttonWithBackgroundImage();
    } else {
      return this.buttonWithoutBackgroundImage();
    }
  }

  render() {
    return (this.renderButton())
  }
}


const styles = StyleSheet.create({
  buttonContainer: {},
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    height: 45,
    width: 157,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 16,
    letterSpacing: 2
  }
});
