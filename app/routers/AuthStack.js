import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";

import { Splash } from '../screens/Auth/Splash';
import { SignIn } from '../screens/Auth/SignIn';
import { MainStack } from './MainStack';
import { Filters } from '../screens/Filters';

const Stack = createStackNavigator();

export const AuthStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
      <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
      <Stack.Screen name="Filters" component={Filters} options={{ headerShown: false }} />
      <Stack.Screen name="MainStack" component={MainStack} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}
