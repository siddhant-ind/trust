import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Text,
} from 'react-native';

export const Splash = ({ navigation }) => {
  setTimeout(() => navigation.navigate('SignIn'), 2000)

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require('../../../assets/logo.png')}
        resizeMode="stretch"
        style={styles.imageBackground}>
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1A1F22",
    justifyContent:'center',
    alignItems:'center'
  },
  imageBackground: {
    height:57,
    width:182
   

  }
});
