import React, { Component } from 'react';
import {
    Alert,
    View,
    ScrollView,
    AsyncStorage,
    TouchableOpacity,
    Text,
    Image,
    KeyboardAvoidingView,
    Linking,
    Platform,
} from 'react-native';
import Color from "../../lib/Color";
import {_components} from "../../components/index";
import {_images} from "../../lib/Images";


export const SignIn = ({ navigation }) => {
    return (
        <ScrollView style={styles.Scroll}>
            <View style={{ flex: 1, marginTop: 180}}>
                <View >
                     <_components.Logo/>
                 </View>
                <View style={styles.form}>
                    <Text style={styles.logoText}>Sign In</Text>
                    <Text style={styles.TEXT}>id</Text>
                    <_components.TRTextInput
                             placeholder={'id'}
                    />
                    <Text style={styles.TEXT}>Password</Text>
                    <_components.TRTextInput
                        placeholder={'Password'}
                        style={{ color: 'white' }} 
                    />
                    <TouchableOpacity><Text style={{ fontSize: 15, marginTop: 10, color: '#1AE1D7' }}>Forgot Password?</Text></TouchableOpacity>
                </View>
                <View style={styles.btn}>
                <_components.TRButton
                        onPress={() => navigation.navigate('MainStack')}
                        text={'Login'}
                        backgroundImage={_images.Rectangle922}
                    />
                    <View style={{ flexDirection: 'row', marginTop: 30 }}>
                        <Text style={{ fontSize: 15, color: 'white' }}> Don't Have An Account?</Text><TouchableOpacity><Text style={{ fontSize: 15, color: '#1AE1D7' }}>Inscription</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}



const styles = {
    Scroll: {
        flex: 1, 
        backgroundColor: '#28313A'
    },
    logoContainer: {
        flex: 0.3, 
        alignItems: 'center', 
        justifyContent: 'flex-end'
    },
    form: {
        flex: 0.35, 
        margin: 15, 
        marginLeft: 30, 
        marginRight: 30, 
        justifyContent: 'center' 
    },
    logoText: {
        fontSize: 25, 
        color: 'white', 
        fontWeight: 'bold',
        marginTop:20
    },
    TEXT: {
        fontSize: 15, 
        marginTop: 20, 
        color: 'white',
        marginBottom:10
    },
    btn: {
        flex: 0.35, 
        alignItems: 'center',
         marginTop: 10 
    },
};
