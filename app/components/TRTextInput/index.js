import React from 'react';
import {
    Image, StyleSheet, View, TextInput

} from "react-native";
import { color } from 'react-native-reanimated';
import Icons from "../../lib/Icons";

      
export default class TRTextInput extends React.Component {
    render() {
        return (
            <View style={[styles.textInputContainer, this.props.textInputContainerStyle]}>
                <View style={[styles.iconContainer, this.props.iconContainerStyle]}>
                    <Image source={this.props.icon} style={[styles.icon, this.props.iconStyle]}/>
                </View>
                <TextInput
                    style={[styles.textInput, this.props.textInputStyle]}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={'#ACACAC'}
                    value={this.props.value}
                    onChangeText={this.props.onChangeText}
                    secureTextEntry={this.props.secureTextEntry}
                    keyboardType={this.props.keyboardType}
                />
                <View style={[styles.rightIconContainer, this.props.rightIconContainerStyle]}>
                    <Image source={this.props.rightIcon} style={[styles.rightIcon, this.props.rightIcon]}/>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    textInputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        height: 45,
        paddingHorizontal: 17,
        borderRadius: 5,
        borderColor: '#ACACAC',
    },
    iconContainer: {},
    icon: {},
    textInput: {
        flex: 1,
        marginLeft: 10,
    },
    rightIconContainer: {},
    rightIcon: {},
});
