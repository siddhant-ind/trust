export const API_URL = 'https://api-test.macoiffeuseafro.com:443/api/v1/interface/'; // prod
// export const API_URL = 'https://api-dev.macoiffeuseafro.com:443/api/v1/interface/'; // dev

export const STORAGE = {
  USER: 'USER',
  HEADERS: 'HEADERS'
};

export const COLORS = {
  PRIMARY: "#28313A",
  SECONDARY: "#68C8C7",
  BORDER: "#ACACAC",
  WHITE: "#FFFFFF",
  BLACK: "#000000"
};
