import React from 'react';
import {Text, View, FlatList, Image, Dimensions, TouchableOpacity} from 'react-native'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {_images} from "../../lib/Images";


export default class Logo extends React.Component {

    render() {
        return (
            <Image
                source={_images.logo}
                style={styles.logoStyle}
            />
        );
    }
}
const styles = {
    logoStyle: {
        height: wp('15%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    }

}