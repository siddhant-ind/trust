export const _images = {
    splash: require('../../assets/splash.png'),
    bell: require('../../assets/bell.png'),
    Ellipse: require('../../assets/Ellipse.png'),
    folder: require('../../assets/folder.png'),
    logo: require('../../assets/logo.png'),
    logosmall: require('../../assets/logosmall.png'),
    Trace: require('../../assets/Trace.png'),
    Rectangle922: require('../../assets/Rectangle922.png'),
    
};

// export default Images;
