import React, { useState } from "react";
import {
    View,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Text,
    Button
} from 'react-native';
import ItemText from '../configs/design/text';
import { _components } from "../components/index";
import DateTimePickerModal from "react-native-modal-datetime-picker";


export const Filters = () => {

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", date);
        hideDatePicker();
    };
    return (

        <_components.Container>

            <_components.AppHeader
            // body={ItemText.Filters()}
            />
            <Text style={styles.logoText}>Filters</Text>
            <View style={styles.form}>

                <Text style={styles.TEXT}>Submission number</Text>
                <_components.TRTextInput
                    // icon={Icons.group_6519}
                    placeholder={'Enter a submission number'}
                    textInputContainerStyle={{ backgroundColor: 'white' }}

                // value={this.state.email}
                // onChangeText={email => this.setState({email})}
                />
                <Text style={styles.TEXT}>User</Text>
                <_components.TRTextInput
                    // icon={Icons.group_6519}
                    textInputContainerStyle={{ backgroundColor: 'white' }}
                    placeholder={'Select an user'}
                    style={{ color: 'white' }}
                // value={this.state.email}
                // onChangeText={email => this.setState({email})}
                />
                <Text style={styles.TEXT}>Assigned to</Text>
                <_components.TRTextInput
                    // icon={Icons.group_6519}
                    textInputContainerStyle={{ backgroundColor: 'white' }}
                    placeholder={'Select a worker'}
                    style={{ color: 'white' }}
                // value={this.state.email}
                // onChangeText={email => this.setState({email})}
                />
                                {/* <View style={{width:90,height:90}}>
                    <Button title="Show Date Picker" onPress={showDatePicker}  />
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                    </View> */}

            </View>




        </_components.Container>

    );
}

const styles = {
    Scroll: {
        flex: 1,
        backgroundColor: '#28313A'
    },
    logoContainer: {
        flex: 0.3,
        alignItems: 'center',
        justifyContent: 'flex-end',

    },
    form: {
        flex: 0.35,
        margin: 15,
        marginTop: 20,
        marginLeft: 30,
        marginRight: 30,
        justifyContent: 'center'
    },
    logoText: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold',
        marginLeft: 30,
        marginTop: 10,
        marginBottom: 10

    },
    TEXT: {
        fontSize: 15,
        marginTop: 15,
        color: '#F9FAFF',
        marginBottom: 10
    },
    btn: {
        flex: 0.35,
        alignItems: 'center',
        marginTop: 10
    },
};
