import React, { useState } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    TextInput
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export const SearchBar = (props) => {
    const [state, setState] = useState({
        searchText: ''
    });

    return (
        <View style={[styles.search, props.searchStyle]}>
            <View style={[
                styles.searchLeftComponent,
                props.searchLeftComponentStyle]}>
                <Icon
                    name={props.searchLeftComponentIcon}
                    size={props.searchLeftComponentIconSize || 20}
                    color={props.searchLeftComponentIconColor || 'grey'} />
            </View>
            <View style={[
                styles.searchMiddleComponent,
                props.searchLeftComponentStyle]}>
                <TextInput
                style={{fontSize:20,backgroundColor:'white'}}
                    maxLength={50}
                    autoCorrect={false}
                    value={state.searchText}
                    onChangeText={(searchText) => {
                        setState({ ...state, searchText })
                        props.onChangeText(searchText)
                    }}
                />
            </View>
            <TouchableOpacity
                activeOpacity={0.8}
                style={[
                    styles.searchRightComponent,
                    props.searchRightComponentStyle]}
                onPress={() => {
                    setState({ ...state, searchText: '' })
                    props.onClear()
                }}
            >
                <Icon
                style={{backgroundColor:'white'}}
                    name={props.searchRightComponentIcon || 'close-circle'}
                    size={props.searchRightComponentIconSize || 20}
                    color={props.searchRightComponentIconColor || 'grey'} />
                    
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    search: {
        height: 40,
        width: 250,
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: '#1A1F22',
        backgroundColor: 'white',
        marginLeft:10,
        marginTop:10
    },
    searchLeftComponent: {
        flex: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // borderTopLeftRadius: 20,
        // borderBottomLeftRadius: 20,
        // backgroundColor: 'red'
    },
    searchMiddleComponent: {
        flex: 80,
        backgroundColor: 'white',
        justifyContent:'center'
    },
    searchRightComponent: {
        flex: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // borderTopRightRadius: 20,
        // borderBottomRightRadius: 20,
        backgroundColor: 'white',
    }
});
