import Container from './Container';
import ResponsiveText from './ResponsiveText';
import AppHeader from './AppHeader';
import TRTextInput from './TRTextInput/index';
import AuthInput from './AuthInput';
import Logo from './Logo';
import InputField from './InputField';
import TRButton from './TRButton';
import TabSwitch from './TabSwitch';
import SearchBar from './SearchBar';



export const _components = {
    Container,
    ResponsiveText,
    AppHeader,
    AuthInput,
    InputField,
    TRButton,
    Logo,
    TRTextInput,
    TabSwitch,
    SearchBar,
}