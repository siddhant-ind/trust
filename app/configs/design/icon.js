import React from 'react';
import {Image} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';



const Icons = {

// -------------Login---------------

  // PersonAuth: (style = []) => <Image source={personAuth} style={style}/>,
  // ShowIcon: (style = []) => <Image source={showIcon} style={style}/>,
  // Mail: (style = []) => <Image source={mail} style={style}/>,
  // LockFill: (style = []) => <Image source={lockFill} style={style}/>,
  // RightArrow: (style = []) => <Image source={rightArrow} style={style}/>,


//  --------Home Icons-----------

  // Pen: (style = []) => <Image source={pen} style={style}/>,
  // Earnings: (style = []) => <Image source={earnings} style={[style, styles.homeIconStyle]}/>,
  // Plannings: (style = []) => <Image source={plannings} style={style}/>,
  // Discussions: (style = []) => <Image source={discussions} style={style}/>,
  // Benefits: (style = []) => <Image source={benefits} style={style}/>,
  // Bookings: (style = []) => <Image source={bookings} style={style}/>,
  // Profile: (style = []) => <Image source={profile} style={style}/>,
  // ArrowUp: (style = []) => <Image source={arrowUp} style={style}/>,
  // More: (style = []) => <Image source={more} style={style}/>,
  // Message: (style = []) => <Image source={message} style={style}/>,
  // Time: (style = []) => <Image source={time} style={[style, styles.time]}/>,
  // Mobile: (style = []) => <Image source={mobile} style={style}/>,
  // Pin: (style = []) => <Image source={pin} style={style}/>,
  // Fb: (style = []) => <Image source={fb} style={style}/>,
  // Reply: (style = []) => <Image source={reply} style={style}/>,
  // Star: (style = []) => <Image source={star} style={style}/>,
  // Tick: (style = []) => <Image source={tick} style={style}/>,
  // ButtonAdd: (style = []) => <Image source={buttonAdd} style={style}/>,
  // ServiceAdded: (style = []) => <Image source={serviceAdded} style={style}/>,
  // LeftTint: (style = []) => <Image source={leftTint} style={style}/>,
  // RightTint: (style = []) => <Image source={rightTint} style={style}/>,
  // Lock: (style = []) => <Image source={lock} style={style}/>,
  // Card: (style = []) => <Image source={card} style={style}/>,
  // AddItem: (style = []) => <Image source={addItem} style={style}/>,


//  Profile Icons

  // Left: (style = []) => <Image source={left} style={style}/>,
  // LeftWhite: (style = []) => <Image source={leftWhite} style={style}/>,
  // StarProfile: (style = []) => <Image source={starProfile} style={style}/>,
  // Cup: (style = []) => <Image source={cup} style={style}/>,
  // Online: (style = []) => <Image source={online} style={style}/>,
  // TickFill: (style = []) => <Image source={tickFill} style={style}/>,
  // AddPhoto: (style = []) => <Image source={addPhoto} style={style}/>,
  // Down: (style = []) => <Image source={down} style={style}/>,
  // Tick1x: (style = []) => <Image source={tick1} style={style}/>,
  // Filter: (style = []) => <Image source={filter} style={style}/>,
  // Cross: (style = []) => <Image source={cross} style={style}/>,
  // Search: (style = []) => <Image source={search} style={style}/>,
  // Bin: (style = []) => <Image source={bin} style={style}/>,
  // Verified: (style = []) => <Image source={verified} style={style}/>,
  // Rejected: (style = []) => <Image source={rejected} style={style}/>,
  // Instagram: (style = []) => <Image source={instagram} style={style}/>,
  // CrossCircle: (style = []) => <Image source={crossCircle} style={style}/>,
  // Disconnect: (style = []) => <Image source={disconnect} style={style}/>,

  //chat
  // Send: (style = []) => <Image source={send} style={style}/>,


};

const styles = {
  homeIconStyle: {
    height: hp('5%'),
    resizeMode: 'contain',
  },
  time: {
    // height: hp('5%'),
    // resizeMode: 'contain',
  }
}


export default Icons;
